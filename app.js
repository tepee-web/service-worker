if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js')
      .then(response => {
        console.log('registration successful', response.message);
        return navigator.serviceWorker.ready;
      }, error => {
        console.info(error.message);
      })
      .then(registration => console.log(registration));
  })
}