const cacheName = 'v2';

const cacheAssets = [
  '/index.html',
  '/app.js',
];

self.addEventListener('install', e => {
  e.waitUntil(
    caches
      .open(cacheName)
      .then(cache => cache
        .addAll(cacheAssets)
        .then(() => self.skipWaiting()))
  )
});

self.addEventListener('activate', e => {
  e.waitUntil(
    caches.keys()
      .then(cacheNames => Promise.all(
        cacheNames.map(cache => {
          console.log('sw: clearing old cache');
          if (cache !== cacheName) return caches.delete(cache);
        })
      ))
  );
});

self.addEventListener('fetch', e => {
  e.respondWith(fetch(e.request).catch(() => caches.match(e.request)))
});